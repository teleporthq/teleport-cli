#!/usr/bin/env node

import chalk from 'chalk'
import commands from './commands'

const VERSION = '0.0.1'

// get the first command if any, otherwise inject the help command
const command = process.argv[2] || 'help'

console.log(chalk.magenta('teleport-cli v' + VERSION))
// if the command exists, execute it
if (commands[command]) {
  // ... and remove current command before passing to next function
  process
    .argv
    .splice(2, 1)

  const output = commands[command](process.argv)
  // if it has an output, display it
  if (output) {
    console.log(output)
  }
} else {
  console.log(`${chalk.cyan(command)} is an unknown command`)
  console.log(`Type \`teleport help\` to see all available commands `)
}
