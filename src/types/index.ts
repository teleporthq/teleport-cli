export type Command = (args?: string[]) => string | void
export interface Commands {
  [commandName: string]: Command
}
