const LOG_FILE = 'teleport.log'
const REPO_DASHBOARD = 'git@gitlab.com:teleporthq/teleport-dashboard.git'
const REPO_LOCAL_SERVER = 'git@gitlab.com:teleporthq/teleport-sketch-server.git'
const REPO_TEST = 'git@gitlab.com:teleporthq/teleport-test.git'

export {
  LOG_FILE,
  REPO_DASHBOARD,
  REPO_LOCAL_SERVER,
  REPO_TEST
}
