import configstore from 'configstore'
import path from 'path'
import { LOG_FILE } from './conf'

const namespace = './teleport'
const store = new configstore(namespace)

const server = {
  getPid: () => store.get('serverPid'),
  setPid: (pid: number) => store.set({serverPid: pid})
}

export default {
  logFilePath : `${path.dirname(store.path)}/${LOG_FILE}`,
  server,
  storePath : store.path
}
