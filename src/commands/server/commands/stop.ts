import chalk from 'chalk'
import {spawnSync} from 'child_process'
import ora from 'ora'
import getpid from './utils/getServerPid'

export default function stop() {
  const pid = getpid()

  // if we get a port it means that the process was running
  if (pid > 0) {
    ora(`server detected (pid ${pid})`).succeed()
    // kill it
    const child2 = spawnSync(`kill -9 ${pid}`, [], {shell: true})
    if (child2.stderr.toString().length === 0) {
      ora('server has been stopped successfully').succeed()
    }
  } else {
    ora('server is not running').info()
  }
}
