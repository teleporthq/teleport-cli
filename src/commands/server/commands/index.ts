import {Commands} from "../../../types"
import help from "./help";
import start from "./start";
import status from "./status";
import stop from "./stop";

const commands: Commands = {
  help,
  start,
  status,
  stop
};

export default commands;
