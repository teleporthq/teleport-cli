import {spawn, spawnSync} from 'child_process'
import fs from 'fs'
import ora from 'ora'
import path from 'path'
import store from '../../../configstore'
import getServerPid from './utils/getServerPid'
const out = fs.openSync(store.logFilePath, 'a')
const err = fs.openSync(store.logFilePath, 'a')

export default function start(args?: string[]) {
  let port = '8000'

  // check if the server is already running
  const pid = getServerPid()
  if (pid > 0) {
    ora(`server is already started (pid ${pid})`).succeed()
    return
  }

  // otherwise, check the port option
  if (args && args.indexOf('-p') !== -1) {
    const portOption = args[args.indexOf('-p') + 1]
    if (!portOption) {
      ora(`missing the port number`).warn()
      return
    }

    if (!portOption.match(/[0-9]+/)) {
      ora(`\`${portOption}\` is not a correct port format`).warn()
      return
    }

    port = portOption
  }

  // spawn and detach server process
  const child = spawn('node', [
    path.resolve(path.dirname(__filename), 'exe', 'server.js'),
    port
  ], {
    detached: true,
    stdio: ['ignore', out, err]
  })

  const spinner = ora(`starting server...`).info();

  setTimeout(() => {
    const child1 = spawnSync(`curl http://localhost:${port}`, [], {shell: true})
    if (child1.stdout.toString().length > 0) {
      spinner.succeed(`server has been started (pid ${child.pid})`)
    } else {
      spinner.fail(`server has not been started. Please check the logs`)
    }
  }, 1000)

  child.unref()
}

export function test() {
  //
}
