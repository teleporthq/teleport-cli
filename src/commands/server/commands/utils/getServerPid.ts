import {spawnSync} from 'child_process'

// tslint:disable-next-line:max-line-length
const child1 = spawnSync(`ps -e | grep teleport-cli/dist/commands/server/commands/exe/server.js [0-9]`, [], {shell: true})
// console.log('stderr', child1.stderr.toString())
// console.log(child1.stdout.toString())
const pid = parseInt(child1.stdout.toString(), 10)

export default function getpid() {
  return pid
}
