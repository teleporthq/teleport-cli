import {spawnSync} from 'child_process'

// tslint:disable-next-line:max-line-length
const child1 = spawnSync(`ps -aef | grep 'teleport-cli/dist/commands/server/commands/exe/server.js$' | awk '{print $2}'`, [], {shell: true})
const pid = parseInt(child1.stdout.toString(), 10)

export default function getpid() {
  return pid
}
