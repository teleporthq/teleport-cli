import chalk from "chalk";
import {spawnSync} from "child_process";
import ora from "ora";

import getServerPid from "./utils/getServerPid";

export default function status() {
  const pid = getServerPid();
  if (pid > 0) {
    ora(`server is running (pid ${pid})`).info();
  } else {
    ora(`server is not running`).info();
  }
}
