#!/usr/bin/env node

import http from 'http'
import store from '../../../../configstore'

const port = parseInt(process.argv[2], 10) || 8000
// console.log('exe port', process.argv) if called as subprocess, all the
// console.log data will be pushed to the log file
const srv = http.createServer((req, res) => {
  console.log(req.url)
  res.write(`I'm the demo server`)
  res.end()
})

// start server
srv.listen(port, 'localhost', () => {
  console.log('[I] ser')
  store
    .server
    .setPid(process.pid)
})

// log errors
srv.on('error', (err : {
  errno: string
}) => {
  console.log(`[E] ${err.errno}`)
})

// on close
srv.on('close', () => {
  store
    .server
    .setPid(0)
})
