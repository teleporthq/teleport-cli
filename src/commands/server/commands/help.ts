import chalk from 'chalk'

export default function help() : string {
  return `  
  ${chalk.bold('SERVER MODULE INSTRUCTIONS')}
    teleport server module is a standalone server which acts as a router/proxy for all messages sent locally by teleport libs

  ${chalk.bold('SYNTAX')}
    teleport server [command] [options]

  ${chalk.bold('COMMANDS')}

    start                   starts the server
    status                  display the status of all the modules
    stop                    stops the server
  `
}
