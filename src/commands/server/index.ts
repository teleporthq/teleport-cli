import chalk from 'chalk'
import ora from 'ora'

import commands from './commands'

export default function server(args?: string[]) {
  // if args is defined, this function is invoked by a parent function
  const argv = args || process.argv
  const command = argv[2] || 'help'

  // if the command exists, execute it
  if (commands[command]) {
    const output = commands[command](process.argv)
    // if it has an output, display it
    if (output) {
      console.log(output)
    }
  } else {
    console.log(`> \`${command}\` is an unknown command`)
    console.log(`> type \`teleport server help\' to see all available commands`)
  }
}
