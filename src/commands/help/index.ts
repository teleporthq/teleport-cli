import chalk from 'chalk'
import {Command} from '../../types'
export default function help() {
  return `${chalk.bold('INSTRUCTIONS')}
    teleport-cli allows you to install and manage the 3 main modules of the teleport ecosystem
  ${chalk.bold('SYNTAX')}
    teleport [command] [options]
  ${chalk.bold('COMMANDS')}
    init                    install hub, dashboard, and previewer modules, the start
    start                   starts all modules
    start [module_name]     starts the specified module (hub|dashboard|previewer)
    status                  display the status of all the modules
    status [module_name]    display the status of the specified module (hub|dashboard|previewer)
    stop                    stops all modules
    stop [module_name]      stops the specified module (hub|dashboard|previewer)
  `
}
