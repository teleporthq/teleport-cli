import { spawn, spawnSync } from 'child_process'
import npm from 'npm'
import ora from 'ora'
import { REPO_DASHBOARD, REPO_LOCAL_SERVER, REPO_TEST } from '../../conf'

function getModuleName(module: string) {
  const name = module.split('/').pop()
  return typeof name !== 'undefined'
    ? name.split('.').shift()
    : name
}

function installModule(module: string) {
  return new Promise((resolve, reject): void => {
    const name = getModuleName(module)
    ora(`installing ${name}...`).info()
    npm.load({
      global: true,
      loglevel: 'silent'
    }, () => {
      const command = `git+ssh://${module}`
      npm.commands.install([command], (err, data) => {
        if (err) {
          reject(err)
          ora(`an error has occured: ${err.message}`).warn()
        } else {
          resolve(data)
          ora(`${data[0][0]} installed succesfully\n`).succeed()
        }
      })
    })
  })
}

export default async function init() {
  await installModule(REPO_TEST)
  await installModule(REPO_LOCAL_SERVER)
  await installModule(REPO_DASHBOARD)
}
