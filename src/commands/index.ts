import {Command, Commands} from '../types/index'
import help from './help'
import init from './init'
import server from './server'

const commands: Commands = {
  help,
  init,
  server
}

export default commands
