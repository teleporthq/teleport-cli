import mainHelp from '../commands/help'
import store from '../configstore'

test('store', () => {
  store
    .server
    .setPid(1)
  expect(store.server.getPid()).toBe(1)
})
