import {spawnSync} from 'child_process'
import help from '../commands/help'

test('command: teleport', () => {
  const child = spawnSync('node', ['dist/index.js'])
  expect(child.stdout.toString().trim).toEqual(help().trim)
})

test('command: teleport help', () => {
  const child = spawnSync('node', ['dist/index.js', 'help'])
  expect(child.stdout.toString().trim).toEqual(help().trim)
})

test('command: teleport unknown_command', () => {
  const child = spawnSync('node', ['dist/index.js', 'unknown_command'])
  expect(child.stdout.toString()).toContain('unknown_command is an unknown command')
  expect(child.stdout.toString()).toContain('Type `teleport help` to see all available commands')
})
